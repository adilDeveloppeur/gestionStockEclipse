package org.sid.dao;

import org.sid.entities.Role;
import org.sid.util.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

	Role findByName(String  name);

}
