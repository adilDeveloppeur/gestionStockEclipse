package org.sid;


import java.util.ArrayList;
import java.util.Arrays;

import org.sid.dao.ProduitRepository;
import org.sid.dao.RoleRepository;
import org.sid.dao.UserRepository;
import org.sid.entities.Produit;
import org.sid.entities.Role;
import org.sid.entities.User;
import org.sid.util.RoleEnum;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GestionStockAngularApplication {

	public static void main(String[] args) {
	ApplicationContext ctx = SpringApplication.run(GestionStockAngularApplication.class, args);
	
	ProduitRepository produitDao = ctx.getBean(ProduitRepository.class);
	
//	produitDao.save(new Produit("Livre",150,10));
//	produitDao.save(new Produit("Cahier",90,15));
//	produitDao.save(new Produit("Cartable",120,25)); 
	
	RoleRepository roleDao = ctx.getBean(RoleRepository.class);
	
	Role roleUser =  new Role(RoleEnum.ROLE_USER);
	Role roleAdmin = new Role(RoleEnum.ROLE_ADMIN);
//	roleDao.save(roleUser);
//	roleDao.save(roleAdmin);

	UserRepository userDao = ctx.getBean(UserRepository.class);
	User user = new User("user", "1234", true);
	User admin =  new User("admin", "1234", true);
	
	user.setRoles(Arrays.asList(roleUser));
//	userDao.save(user);
	
	admin.setRoles(Arrays.asList(roleUser,roleAdmin));
//	userDao.save(admin);
}
}
