package org.sid.mock;

import java.util.List;


import org.springframework.stereotype.Service;

@Service
public interface IService<T, ID> {
	
	
	List<T> getAll();
	public T add(T entity);
	void update(T entity);
	boolean delete(ID id);

}
