package org.sid.mock;

import java.util.ArrayList;
import java.util.List;

import org.sid.entities.Produit;
import org.springframework.stereotype.Service;




@Service
public class ProduitMockServiceImplementation implements IService<Produit, String> {

	private List<Produit> listeProduit;
	
	
	@Override
	public List<Produit> getAll() {
		listeProduit= new ArrayList<>();
		listeProduit.add(new Produit("Livre",150,10));
		listeProduit.add(new Produit("Cahier",90,15));
		listeProduit.add(new Produit("Cartable",120,25));
		return listeProduit;
	}

	@Override
	public Produit add(Produit produit) {
		listeProduit.add(produit);
		return produit;
	}

	@Override
	public void update(Produit produit) {
		listeProduit.remove(produit);
		listeProduit.add(produit);
	
	}

	@Override
	public boolean delete(String reference) {
		Produit produit = new Produit();
		produit.setReference(reference);
		listeProduit.remove(produit);
		return true;
	}

	

}
