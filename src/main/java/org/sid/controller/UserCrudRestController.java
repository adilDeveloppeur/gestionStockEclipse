package org.sid.controller;

import java.util.Arrays;
import java.util.List;

import org.sid.dao.RoleRepository;

import org.sid.entities.Role;
import org.sid.entities.User;

import org.sid.util.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crud_user")
public class UserCrudRestController extends CrudController<User, Long> {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public List<User> getAll() {
		List<User> users = super.getAll();
		users.forEach(user->{
			user.setPassword(null);
		});
		
		return users;
	}


	@Override
	public User save(@RequestBody User user) {
	Role role = roleRepository.findByName(RoleEnum.ROLE_USER.getName());
	user.setRoles(Arrays.asList(role));
	user.setActive(true);
		return super.save(user);
	}
	
	
	
}
