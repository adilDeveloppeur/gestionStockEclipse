package org.sid.controller;

import java.util.List;

import org.sid.entities.Produit;
import org.sid.service.ICrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


public class CrudController<T,ID>  {
	
	@Autowired
	ICrudService<T,ID> service;
	
	@GetMapping
	public List<T> getAll() {
		
		return service.getAll();
	}

	@PostMapping
	public T save(@RequestBody T entity) {
		
		return service.save(entity);
	}
	
	@PutMapping("/{id}")
	public T update(@PathVariable ID id, @RequestBody T entity) {
		return service.save(entity);
	}
	
	@DeleteMapping("/{id}")
	public String delete(@PathVariable ID id) {
		
		service.delete(id);
		return Boolean.TRUE.toString();
	}

}
