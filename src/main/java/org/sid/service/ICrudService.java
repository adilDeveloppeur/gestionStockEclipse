package org.sid.service;

import java.util.List;

public interface ICrudService<T, ID> {
	
	public List<T> getAll();
	public T save(T entity);
	public T update(T entity);
	public String delete(ID id);

}
