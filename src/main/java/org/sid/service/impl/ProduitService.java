package org.sid.service.impl;

import java.util.List;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.sid.service.ICrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProduitService implements ICrudService<Produit, Long> {

	@Autowired
	private ProduitRepository produitRepository;
	
	@Override
	public List<Produit> getAll() {
		
		return produitRepository.findAll();
	}

	@Override
	public Produit save(Produit entity) {
		
		return produitRepository.save(entity);
	}

	@Override
	public Produit update(Produit entity) {
		
		return produitRepository.save(entity);
	}

	@Override
	public String delete(Long id) {
		produitRepository.deleteById(id);
		
		return Boolean.TRUE.toString();
	}

}
