package org.sid.service.impl;

import java.util.List;

import org.sid.dao.UserRepository;
import org.sid.entities.User;
import org.sid.service.ICrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements ICrudService<User, Long> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> getAll() {

		return userRepository.findAll();
	}

	@Override
	public User save(User entity) {

		return userRepository.save(entity);
	}

	@Override
	public User update(User entity) {

		return userRepository.save(entity);
	}

	@Override
	public String delete(Long id) {
		userRepository.deleteById(id);

		return Boolean.TRUE.toString();
	}

}
