package org.sid.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import org.sid.dao.UserRepository;
import org.sid.entities.Role;
import org.sid.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CustumUserDetailService implements UserDetailsService{

	
	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.findUserByUsername(username);
		boolean accountNotExpired= true;
		boolean credentialsNonExpired = true;
		boolean accountNotLocked =true;
		
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(
					username, 
					user.getPassword(), 
					user.isActive(),
					accountNotExpired,
					credentialsNonExpired,
					accountNotLocked,
					getAuthorities(user.getRoles())
				);
	
			return userDetails;
			
	}
		
		private Collection<?extends GrantedAuthority> getAuthorities(List<Role> roles) {
			
			Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			
			for (Role role : roles) {
				GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getName());
				grantedAuthorities.add(grantedAuthority);
				
			}
			
			return grantedAuthorities;
		}
		
		
	

}
